import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyADhDVECgTEZsp5KkRjqiLud2naRhO2dPE",
  authDomain: "itailor-backend.firebaseapp.com",
  projectId: "itailor-backend",
  storageBucket: "itailor-backend.appspot.com",
  messagingSenderId: "135297100964",
  appId: "1:135297100964:web:d9b0a7d7875b5675024a31",
  measurementId: "G-B8YBYC447M"
};

const app = initializeApp(firebaseConfig);

const auth = getAuth(app)
const fireStoreConfig = getFirestore(app)
const storageConfig = getStorage(app)


export {auth, fireStoreConfig, storageConfig}
