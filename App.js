import { useColorScheme } from 'react-native';
import { BaseNavigator } from "./src/navigations/baseNavigator";
import { useFonts } from "expo-font";

import { ThemeProvider } from "@rneui/themed";
import { theme } from "./src/utils/theme";

export default function App() {
// use color scheme will give us users' phone theme ( dark/light )
  theme.mode = useColorScheme()
  const [fontsLoaded, fontError] = useFonts({
    "OperatorMono-Bold": require("./assets/fonts/OperatorMono-Bold.otf"),
    "OperatorMono-Book": require("./assets/fonts/OperatorMono-Book.otf"),
    "OperatorMono-Light": require("./assets/fonts/OperatorMono-Light.otf"),
    "OperatorMono-Medium": require("./assets/fonts/OperatorMono-Medium.otf"),
    "OperatorMono-LightItalic": require("./assets/fonts/OperatorMono-LightItalic.otf"),
  });

  if (!fontsLoaded && !fontError) {
    return null;
  }



  return (
    <ThemeProvider theme={theme}>
      <BaseNavigator />
    </ThemeProvider>
  );
}
