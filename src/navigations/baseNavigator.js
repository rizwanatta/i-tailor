import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useTheme } from "@rneui/themed";
import Register from "../screens/Register";
import Tailors from "../screens/Tailors";
import Persons from "../screens/persons/Persons";
import Camera from "../screens/Camera";
import Login from "../screens/login";
import Home from "../screens/home";
import Settings from "../screens/settings";
import Maps from "../screens/maps";

const BaseStack = createNativeStackNavigator();

function BaseNavigator() {

  const {theme} = useTheme()

  
  return (
    <NavigationContainer>
      <BaseStack.Navigator
        screenOptions={{
          headerStyle:{
            backgroundColor: theme.colors.background,
          },
          headerTitleStyle: {
            color: "orange",
            fontFamily: "OperatorMono-Book",
          },
        }}
        initialRouteName={"Login"}
      >
        <BaseStack.Screen name={"Register"} 

          component={Register} />
        <BaseStack.Screen name={"Persons"} component={Persons} />
        <BaseStack.Screen name={"Tailors"} component={Tailors} />
        <BaseStack.Screen name={"Login"} component={Login} />
        <BaseStack.Screen name={"Home"} component={Home} />
        <BaseStack.Screen name={"Settings"} component={Settings} />
        <BaseStack.Screen name={"Maps"} component={Maps} />
        <BaseStack.Screen
          options={{ headerShown: false }}
          name={"Camera"}
          component={Camera}
        />
      </BaseStack.Navigator>
    </NavigationContainer>
  );
}

export { BaseNavigator };
