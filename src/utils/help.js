import PasswordValidator from "password-validator";
// import fetch from 'fetch';

function validatePassword(password) {
  // Create a schema
  var schema = new PasswordValidator();

  // Add properties to it
  schema
    .is()
    .min(5) // Minimum length 8
    .is()
    .max(100) // Maximum length 100
    .has()
    .not()
    .spaces(); // Should not have spaces

  return schema.validate(password);
}

const imgToBlob = async (uri) => {
  try {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", uri, true);
    xhr.responseType = "blob";
    const blobPromise = new Promise((resolve, reject) => {
      xhr.onload = () => {
        const blob = xhr.response;
        resolve(blob);
      };
      xhr.onerror = () => {
        reject(new Error("Failed to load image"));
      };
    });
    xhr.send();
    const blob = await blobPromise;
    return blob;
  } catch (error) {
    console.error(error);
  }
};

const getRandomImageName= (prefix = "img", postfix = "png") => {
  return `${prefix}_${Math.random()}.${postfix}`;
};

export { validatePassword, imgToBlob, getRandomImageName };
