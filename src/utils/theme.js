import { createTheme } from "@rneui/themed";

const fonts = {
  light: "OperatorMono-Light",
  lightItalic: "OperatorMono-LightItalic",
  regular: "OperatorMono-Book",
  medium: "OperatorMono-Medium",
  bold: "OperatorMono-Bold",
};

const theme = createTheme({
  lightColors: {
    primary: "#1E9DA5",
    secondary: "#769553",
    background: "#ffffff",
  },
  darkColors: {
    primary: "#3C4459",
    secondary: "#769553",
    background: "#715295",
  },
  mode: "light",

  components: {
    Button:{
      titleStyle:{
        fontFamily:fonts.medium
      },
      color:'#1E9DA5'
    },
    Input: {
      borderWidth:1,
      borderRadius:5,
      borderColor:'#1E9DA5',
      padding:5,
      style:{
        fontFamily:fonts.medium
      }
    },
  },
});

// FIX ME delete these after setting the above theme

const colors = {
  primary: "#1E9DA5",
  secondary: "#769553",
  accent: "#715295",
  primaryDark: "#3C4459",
  white: "#FFFFFF",
  black: "#000000",
};


export { colors, fonts, theme };
