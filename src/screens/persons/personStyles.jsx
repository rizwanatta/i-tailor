import { StyleSheet } from "react-native";

const personStyles = new StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
  },
  label: {
    color: "pink",
    fontSize: 22,
  },
});

export { personStyles };
