import {
  View,
  Text,
  FlatList,
  Image,
  Button,
  TouchableOpacity,
} from "react-native";
import React, { useState, useEffect } from "react";
import { useTheme } from '@rneui/themed';

import { personStyles } from "./personStyles";

// import { MediaPicker } from "../../components/mediaPicker";

export default function Persons({ navigation }) {
  const [image, setImage] = useState(null);
  const [showMediaPicker, setShowMediaPicker] = useState(false);

  const {theme} = useTheme()

 

  // a useEffect with empty array mocks MOUNTING
  useEffect(() => {
    console.log("mounting");
  }, []);

  // a useEffect with empty array and a return callback mocks UNMOUNTING
  useEffect(() => {
    return () => {
      console.log("unmounting");
    };
  }, []);

  function onBtnPress() {
    navigation.replace("Tailors");
  }

  function onCameraGoTo() {
    navigation.navigate("Camera");
  }

  function handleShowMediaPicker(imageUri) {

    console.log(imageUri)
    if (imageUri!== "") {
      setImage(imageUri);
    }

    // if(showMediaPicker === true ){
    //   setShowMediaPicker(false)
    // }else {
    //   setShowMediaPicker(true)
    // }

    // this line below is same as above logic

    setShowMediaPicker(!showMediaPicker);
  }

  return (
    <View style={{backgroundColor:theme.colors.background, flex:1}}>
      <Button onPress={onBtnPress} title={"go to tailors"}  color={theme.colors.error}/>
      <Button onPress={onCameraGoTo} title={"go to Camera"} />

      <TouchableOpacity onPress={()=>handleShowMediaPicker("")}>
        {image ? (
          <Image source={{ uri: image }} style={{ width: 100, height: 100 }} />
        ) : (
          <Image
            source={require("../../../assets/icon.png")}
            style={{ width: 100, height: 100 }}
          />
        )}
      </TouchableOpacity>

    </View>
  );
}
