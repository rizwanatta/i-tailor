import { StyleSheet, View, ScrollView, Image } from "react-native";
import { Input, Button, Text } from "@rneui/themed";
import React, { useState, useRef, useEffect } from "react";
import * as EmailValidator from "email-validator";
import { useTheme } from "@rneui/themed";
import { signInWithEmailAndPassword } from "firebase/auth";
import Spinner from "react-native-loading-spinner-overlay";
import { auth } from "../../firebaseConfig";

import { validatePassword } from "../utils/help";

export default function Login({ navigation }) {
  const [email, setEmail] = useState("");

  const [emailError, setEmailError] = useState("");
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [showPass, setShowPass] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const { theme } = useTheme();

  const emailRef = useRef();
  const passwordRef = useRef();

  useEffect(() => {}, []);

  function onEyePress() {
    if (showPass === true) {
      setShowPass(false);
    } else if (showPass === false) {
      setShowPass(true);
    }
  }

  const onLoginPress = async () => {
    if (email === "") {
      setEmailError("Field is required");
      emailRef.current.shake();
    }
    if (password === "") {
      setPasswordError("Field is required");
      passwordRef.current.shake();
    }

    if (email) {
      // if user has eneterd something then validate the email this.state.

      const isValidEmail = EmailValidator.validate(email);

      if (isValidEmail === false) {
        setEmailError("Please enter a valid email");
        emailRef.current.shake();
      } else {
        setEmailError("");
      }
    }

    if (password) {
      // if user has something in password Field validate it using our schema of password

      if (validatePassword(password) === false) {
        setPasswordError("this is not a valid password");
        passwordRef.current.shake();
      } else {
        setPasswordError("");
      }
    }

    if (email && password && !passwordError && !emailError) {
      setIsLoading(true);

      // emaila dna pass enetered and no error pressent login please

      try {
        const signinResponse = await signInWithEmailAndPassword(
          auth,
          email,
          password,
        );
        setIsLoading(false);

        if(signinResponse){

          // start a user session
          // take user to home page 
        }

      } catch (error) {
        alert(error.message);
        setIsLoading(false);
      }
    }
  };

  return (
    <ScrollView style={{ backgroundColor: theme.colors.background }}>
      <View style={styles.formCon}>
        <Image
          style={{ width: 100, height: 100, alignSelf: "center" }}
          source={{
            uri: "https://cdn.pixabay.com/photo/2016/06/23/18/55/apple-1475977_1280.png",
          }}
        />
        <Input
          placeholder={"Email"}
          onChangeText={setEmail}
          ref={emailRef}
          value={email}
          error={emailError}
          errorMessage={emailError}
          keyboardType="email-address"
        />
        <Input
          placeholder={"Password"}
          error={passwordError}
          errorMessage={passwordError}
          ref={passwordRef}
          onChangeText={setPassword}
          secureTextEntry={showPass}
          rightIcon={{
            type: "material-community",
            name: showPass ? "cards-heart-outline" : "search",
            color: theme.colors.white,
            onPress: onEyePress,
          }}
        />
        <Button
          size={"lg"}
          loading={isLoading}
          containerStyle={{ width: 300, alignSelf: "center" }}
          title={"Login"}
          onPress={onLoginPress}
        />

        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text> Dont have an account?</Text>
          <Button
            color={"primary"}
            title={"Register"}
            type={"clear"}
            onPress={() => {
              navigation.navigate("Register");
            }}
          />
        </View>
        <Spinner
          visible={isLoading}
          textStyle={{ color: "white" }}
          textContent={"Loading..."}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  formCon: {
    width: "100%",
    padding: 10,
  },
});
