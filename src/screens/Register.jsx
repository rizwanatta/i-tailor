import { StyleSheet, View, ScrollView, Image } from "react-native";
import { Input, Button, Text } from "@rneui/themed";
import React, { useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useTheme } from "@rneui/themed";
import Spinner from "react-native-loading-spinner-overlay";


import { createUserWithEmailAndPassword } from "firebase/auth";
import { doc, setDoc } from "firebase/firestore";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";

import { auth, fireStoreConfig, storageConfig } from "../../firebaseConfig";
import { MediaPicker } from "../components/mediaPicker";
import { Avatar } from "@rneui/base";
import { getRandomImageName, imgToBlob } from "../utils/help";

export default function Register({ navigation }) {
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [userGender, setUserGender] = useState();
  const [showPass, setShowPass] = useState(true);
  const [showMediaPicker, setShowMediaPicker] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dp, setDP] = useState();

  const { theme } = useTheme();

  const onRegisterPress = async () => {
    if (!dp) {
      alert("please attacha picture");
      return;
    }

    setLoading(true);

    try {
      // make blob of my image please
      const blob = await imgToBlob(dp);

      // give picture a name to be stored with and give it config reffrenece of storage

      const storageRef = ref(storageConfig, getRandomImageName("profile_pic","png"));

      const uploadResposne = await uploadBytes(storageRef, blob);
      // this will bring us details of the image that we uploaded and we can use url
      const downlodeableImageUrl = await getDownloadURL(storageRef)


      if (uploadResposne) {
        let response = await createUserWithEmailAndPassword(
          auth,
          email,
          password,
        );
        // this will have the unique ID in !

        const uuid = response.user.uid;

        const docData = {
          firstName: firstName,
          lastName: lastName,
          userGender: userGender,
          email: email,
          profile_pic: downlodeableImageUrl
        };

        let docResponse = await setDoc(
          doc(fireStoreConfig, "users", uuid),
          docData,
        );


        navigation.goBack()
        alert('Account created successfully login now please')
      }
    } catch (error) {
      alert(error.message);
    }

    setLoading(false);
  };

  function onEyePress() {
    if (showPass === true) {
      setShowPass(false);
    } else if (showPass === false) {
      setShowPass(true);
    }
  }

  function goToLogin() {
    navigation.goBack();
  }

  const onMediaPickerClosed = (img) => {
    if (img) {
      setDP(img);
    }
    setShowMediaPicker(!showMediaPicker);
  };

  return (
    <ScrollView style={{ backgroundColor: theme.colors.background }}>
      <View style={styles.formCon}>
        <View style={{ alignSelf: "center", margin: 10 }}>
          <Avatar
            size={"large"}
            rounded
            source={{
              uri:
                dp ||
                "https://cdn.pixabay.com/photo/2016/06/23/18/55/apple-1475977_1280.png",
            }}
            onPress={() => setShowMediaPicker(!showMediaPicker)}
          />
        </View>

        <Input placeholder={"First Name"} onChangeText={setFirstName} />
        <Input placeholder={"Last Name"} onChangeText={setLastName} />
        <Input placeholder={"Email"} onChangeText={setEmail} />
        <Input
          placeholder={"Password"}
          onChangeText={setPassword}
          secureTextEntry={showPass}
          rightIcon={{
            type: "material-community",
            name: showPass ? "cards-heart-outline" : "search",
            color: theme.colors.white,
            onPress: onEyePress,
          }}
        />
        <Input placeholder={"Gender"} onChangeText={setUserGender} />
        <Button
          size={"lg"}
          containerStyle={{ width: 300, alignSelf: "center" }}
          title={"Register"}
          onPress={onRegisterPress}
        />

        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text> Already have an account?</Text>
          <Button
            color={"primary"}
            title={"Signin"}
            type={"clear"}
            onPress={goToLogin}
          />
        </View>

        <MediaPicker
          show={showMediaPicker}
          onClosePressed={onMediaPickerClosed}
        />
      </View>

      <Spinner
        visible={loading}
        textStyle={{ color: "white" }}
        textContent={"Loading..."}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  formCon: {
    width: "100%",
    padding: 10,
  },
});
