import React, { useEffect , useState } from "react";
import MapView, { Marker } from "react-native-maps";
import { StyleSheet, View } from "react-native";
import * as Location from 'expo-location';



function Maps({navigation}) {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);


  useEffect(() => {
    attempToGetUserLocationPermission()
  }, []);


  async function attempToGetUserLocationPermission(){

      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
  }


  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        showsMyLocationButton
        zoomControlEnabled
        showsUserLocation
        zoomEnabled
      >
        <Marker
         title={'yayeeee yaeee'} 
          coordinate={{
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
          }}
        />
      </MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: "100%",
    height: "100%",
  },
});

export default Maps;
