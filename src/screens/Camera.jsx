import { StyleSheet, View, Alert } from "react-native";
import { Camera as ExpoCamera, CameraType } from "expo-camera";
import FontAwesome from "@expo/vector-icons/FontAwesome";

import * as MediaLibrary from "expo-media-library";

import React, { useState } from "react";
import { colors } from "../utils/theme";

const Camera = () => {
  const [type, setType] = useState(CameraType.front);

  const [picTaken, setPicTaken] = useState("");
  const [cameraRef, setCameraRef] = useState();

  const [permission, requestPermission] = ExpoCamera.useCameraPermissions();
  requestPermission();

  function flipCamera() {
    if (type === CameraType.front) {
      setType(CameraType.back);
    } else {
      setType(CameraType.front);
    }
  }

  async function onTakePicture() {
    try {
      // if camera is not ok! as a ref dont do anything
      if (!cameraRef) return;

      const pictureData = await cameraRef.takePictureAsync();
      setPicTaken(pictureData);

      attemptToSavePictureToGallery();
    } catch (error) {
      alert("error.message");
    }
  }

  async function attemptToSavePictureToGallery() {
    try {
      const permission = await MediaLibrary.requestPermissionsAsync();
      if (permission.granted === true) {
        MediaLibrary.saveToLibraryAsync(picTaken.uri);

        Alert.alert("ya title h", "ya message h")

      }
    } catch (error) {
      alert("error.message");
    }
  }

  return (
    <View style={styles.con}>
      <ExpoCamera style={styles.camera} type={type} ref={setCameraRef}>
        <View style={styles.innerCon}>
          <View style={styles.bottomCon}>
            <View
              style={{
                alignItems: "flex-end",
                width: "65%",
                paddingHorizontal: 10,
              }}
            >
              <FontAwesome
                name={"circle"}
                size={80}
                color={colors.white}
                onPress={onTakePicture}
              />
            </View>
            <FontAwesome
              style={{ margin: 10 }}
              name={"refresh"}
              size={50}
              color={colors.white}
              onPress={flipCamera}
            />
          </View>
        </View>
      </ExpoCamera>
    </View>
  );
};

export default Camera;

const styles = StyleSheet.create({
  con: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  innerCon: {
    width: "100%",
    height: "100%",
    justifyContent: "flex-end",
  },

  bottomCon: {
    height: 100,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
