import { View, ScrollView, FlatList } from "react-native";
import React, { useState } from "react";
import {
  FAB,
  Card,
  Text,
  ListItem as ProfileDetails,
  Avatar,
} from "@rneui/themed";
import { ListItem as PersonalDetails } from "@rneui/themed";

const Settings = ({ navigation }) => {
  const [showPersonal, setShowPersonal] = useState(false);

  function attemptToLogout() {
    navigation.reset({ index: 1, routes: [{ name: "Login" }] });
  }

  return (
    <ScrollView>
      <ProfileDetails>
        <ProfileDetails.Content style={{ alignItems: "center" }}>
          <Avatar
            rounded
            size={"xlarge"}
            source={{ uri: "https://randomuser.me/api/portraits/men/16.jpg" }}
          />
          <ProfileDetails.Title> Rizwan </ProfileDetails.Title>
          <ProfileDetails.Subtitle> Trainer Erozgaar </ProfileDetails.Subtitle>
        </ProfileDetails.Content>
      </ProfileDetails>

      <PersonalDetails.Accordion
        style={{ margin: 10 }}
        content={
          <>
            <PersonalDetails.Title>Personal Details</PersonalDetails.Title>
          </>
        }
        isExpanded={showPersonal}
        onPress={() => {
          setShowPersonal(!showPersonal);
        }}
      >
        <PersonalDetails.Content>
          <Text>hie 2sto mera name rizwan i am from JHang </Text>
          <Text>Education technical diploma in computers</Text>
          <Text>I eat anythign asnd everything</Text>
          <Text>120kg</Text>
        </PersonalDetails.Content>
        <PersonalDetails.Chevron color={"white"} />
      </PersonalDetails.Accordion>

      <Text h3 h3Style={{ color: "black" }}>
        {"Friends"}
      </Text>

      <FlatList
        data={[1, 2, 3, 4, 5]}
        horizontal={true}
        renderItem={({ item }) => (
          <Card containerStyle={{ borderRadius: 10, width: 300 }}>
            <Card.Title>hie {item}</Card.Title>
            <Card.Divider />
            <Card.Image
              source={{
                uri: `https://randomuser.me/api/portraits/women/${
                  16 + item
                }.jpg`,
              }}
            />
          </Card>
        )}
      />
      <FAB
        size={"large"}
        color={"red"}
        title={"Logout"}
        onPress={attemptToLogout}
        placement="right"
        icon={{ name: "log-out", type: "ionicon", color: "white" }}
      />
    </ScrollView>
  );
};

export default Settings;
