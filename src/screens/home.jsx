import { StyleSheet, Text, View, FlatList } from "react-native";
import React, { useEffect, useState } from "react";
import { Button, ListItem, Avatar, Icon } from "@rneui/themed";

const tailors = [
  {
    id: 0,
    name: "John Smith",
    location: "123 Main St",
    rating: 4.5,

    image: "https://randomuser.me/api/portraits/men/36.jpg",
  },
  {
    id: 1,
    name: "Jane Doe",
    location: "456 Elm St",
    rating: 3.8,

    image: "https://randomuser.me/api/portraits/men/37.jpg",
  },
  {
    id: 2,
    name: "Mike Johnson",
    location: "789 Oak St",
    rating: 4.2,

    image: "https://randomuser.me/api/portraits/men/38.jpg",
  },
  {
    id: 3,
    name: "Sarah Lee",
    location: "321 Maple Ave",
    rating: 4.9,

    image: "https://randomuser.me/api/portraits/men/39.jpg",
  },
  {
    id: 4,
    name: "David Chen",
    location: "654 Pine St",
    rating: 3.5,

    image: "https://randomuser.me/api/portraits/men/40.jpg",
  },
  {
    id: 5,
    name: "Emily Kim",
    location: "987 Cedar St",
    rating: 4.7,
    image: "https://randomuser.me/api/portraits/men/41.jpg",
  },
  {
    id: 6,
    name: "Alex Wong",
    location: "246 Birch Ave",
    rating: 4.1,
    image: "https://randomuser.me/api/portraits/men/42.jpg",
  },
  {
    id: 7,
    name: "Jessica Liu",
    location: "135 Spruce St",
    rating: 3.9,

    image: "https://randomuser.me/api/portraits/women/43.jpg",
  },
  {
    id: 8,
    name: "Ryan Lee",
    location: "864 Walnut Ave",
    rating: 4.3,
    image: "https://randomuser.me/api/portraits/men/44.jpg",
  },
  {
    id: 9,
    name: "Karen Chen",
    location: "579 Cherry St",
    rating: 4.6,
    image: "https://randomuser.me/api/portraits/men/45.jpg",
  },
  {
    id: 10,
    name: "Tommy Kim",
    location: "291 Pine Ave",
    rating: 3.7,
    image: "https://randomuser.me/api/portraits/men/46.jpg",
  },
];

const Home = ({ navigation }) => {
  const [data, setData] = useState(tailors);


  function goToSettings(){
    navigation.navigate('Settings')
  }

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
      <View style={{alignItems: 'center',justifyContent: 'center',flexDirection:'row'}}>
        <Icon size={20} color="green" type={"ionicon"} name={"person"} />
        <Icon
 onPress={goToSettings}

            size={20} color="yellow" type={"ionicon"} name={"settings"} />
        </View>
      ),
    });
  }, [navigation]);

  function __renderItem({ item, index }) {
    function deleteSelf() {
      const dummyTailors = [...data];
      // delete an item from old array
      dummyTailors.splice(index, 1);
      // new array again add it to our state
      setData(dummyTailors);
    }

    return (
      <ListItem.Swipeable
        style={{ margin: 5 }}
        rightContent={(action) => (
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <Icon
              onPress={deleteSelf}
              raised
              size={35}
              color="green"
              type={"ionicon"}
              name={"trash"}
            />
          </View>
        )}
      >
        <Avatar rounded={true} size={"large"} source={{ uri: item.image }} />
        <ListItem.Content>
          <ListItem.Title>{item.name}</ListItem.Title>
          <ListItem.Subtitle>{item.location}</ListItem.Subtitle>
        </ListItem.Content>
        <ListItem.Chevron size={33} name="heart" type={"ionicon"} />
      </ListItem.Swipeable>
    );
  }

  return (
    <>
      <FlatList
        data={data}
        keyExtractor={(item) => item.id}
        renderItem={__renderItem}
      />
    </>
  );
};

export default Home;

const styles = StyleSheet.create({});
