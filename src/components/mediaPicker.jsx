import { StyleSheet,  View, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import Modal from "react-native-modal";
import Ionicons from "@expo/vector-icons/Ionicons";
import * as ImagePicker from "expo-image-picker";

/**
 * function MediaPicker
 * onClosePressed an action to close the StyleSheet
 *  - you will need a state and its changer to work with this
 * show: a prop that hides or shows the model sheet
 * **/

const MediaPicker = ({ onClosePressed, show = false }) => {

  const [picTaken, setPicTaken] = useState(null);


  ImagePicker.requestMediaLibraryPermissionsAsync();

  async function openGalleryPicker() {
    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 0.5,
      });

      if (result.canceled === false) {
        const imgUrl = result.assets[0].uri
        setPicTaken(imgUrl);
        onClosePressed(imgUrl)
      }
      console.log(result);
    } catch (error) {
      console.log(error);
    }
  }

  async function openCamera() {
    try {
      const result = await ImagePicker.launchCameraAsync({
        allowsEditing: true,
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 0.5,
      });

      if (result.canceled === false) {
        const imgUrl = result.assets[0].uri
        setPicTaken(imgUrl);
        onClosePressed(imgUrl)
      }
      console.log(result);
    } catch (error) {
      console.log(error);
    }
  }


  return (
    <View style={styles.con}>
      <Modal
        isVisible={show}
        style={{
          justifyContent: "flex-end",
          margin: 0,
        }}
      >
        <View style={{ backgroundColor: "white", height: "20%" }}>
          <Ionicons
            name={"close-circle"}
            color="red"
            size={32}
            onPress={onClosePressed}
          />

          <View style={styles.buttonRow}>
            <TouchableOpacity style={styles.btnCon} onPress={openCamera} >
              <Ionicons name={"camera-sharp"} color={"white"} size={50} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.btnCon} onPress={openGalleryPicker}>
              <Ionicons name={"images-sharp"} color={"white"} size={50} />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export { MediaPicker };

const styles = StyleSheet.create({
  con: {
    flex: 1,
    padding: 0,
  },
  buttonRow: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  btnCon: {
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: "orange",
    justifyContent: "center",
    alignItems: "center",
  },
});
